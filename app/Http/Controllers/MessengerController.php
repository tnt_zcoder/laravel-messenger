<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Models\User;

class MessengerController extends BaseController
{
    /**
     * @return View
     */
    public function index(): View
    {
        $users = [];
        if (Auth::user()) {
            $users = User::where('id', '<>', Auth::user()->id)->get();
        }
        return view('messenger.index', [
            'users' => $users,
        ]);
    }

    /**
     * @return View
     */
    public function inbox(): View
    {
        $users = [];
        if (Auth::user()) {
            $users = User::where('id', '<>', Auth::user()->id)->get();
        }
        return view('messenger.inbox', [
            'users' => $users,
        ]);
    }
}

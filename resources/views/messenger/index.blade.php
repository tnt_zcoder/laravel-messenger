@extends('layouts.app')

@section('content')
<div class="flex w-full h-screen overflow-hidden antialiased text-gray-800 bg-white">
    <div class="flex flex-col flex-1">
        <div class="z-20 flex flex-row">
            @include('inc.navbar')
        </div>
        @guest
            <div class="container flex flex-col justify-center w-full h-screen mx-auto">
                <h1 class="text-6xl font-bold text-red-600">Please login/ register to continue</h1>
            </div>
        @endguest
        @auth
            <main class="flex flex-row flex-grow min-h-0">
                @include('messenger.inc.sidebar-conversation', ['users' => $users])

                <div class="container flex flex-col justify-center w-full h-screen mx-auto">
                    <h1 class="text-6xl font-bold text-center text-blue-900">Click choose user to Start Conversations</h1>
                </div>
            </main>
        @endauth
    </div>
</div>
@endsection


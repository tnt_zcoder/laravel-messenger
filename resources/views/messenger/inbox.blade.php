@extends('layouts.app')

@section('content')
<div class="flex w-full h-screen overflow-hidden antialiased text-gray-800 bg-white">
    <div class="flex flex-col flex-1">
        <div class="z-20 flex flex-row">
            @include('inc.navbar')
        </div>
        @auth
            <input id="user-auth" value="{{ Auth::user()->id }}" type="hidden">
            <main class="flex flex-row flex-grow min-h-0">
                @include('messenger.inc.sidebar-conversation', ['users' => $users])

                <section class="flex flex-col flex-auto border-l" x-data="inbox()" x-init="fetchUser()">

                    <template x-if="loading">
                        <div class="fixed top-0 bottom-0 left-0 right-0 z-50 flex flex-col items-center justify-center w-full h-screen overflow-hidden bg-gray-700 opacity-75">
                            <div class="w-12 h-12 mb-4 ease-linear border-4 border-t-4 border-gray-200 rounded-full loader"></div>
                            <h2 class="text-xl font-semibold text-center text-white">Loading...</h2>
                            <p class="w-1/3 text-center text-white">This may take a few seconds, please don't close this page.</p>
                        </div>
                    </template>

                    <input type="hidden" value="{{ request('id') }}" id="user-id">
                    <div class="flex flex-row items-center justify-between flex-none px-6 py-4 shadow chat-header">
                        <template x-if="user_info">
                            <div class="flex">
                                <div class="relative flex flex-shrink-0 w-12 h-12 mr-4">
                                    <img class="object-cover w-full h-full rounded-full shadow-md" :src="user_info.base.profile_photo_url" />
                                </div>
                                <div class="mt-1 text-sm">
                                    <p class="font-bold" x-text="user_info.name"></p>
                                    <p x-text="user_info.base.email"></p>
                                </div>
                            </div>
                        </template>
                    </div>

                    <div class="flex-1 p-4 overflow-y-scroll chat-body">
                        <template x-if="!thread_id">
                            <h1 class="text-2xl font-bold text-center text-blue-900">Send message to create Conversation</h1>
                        </template>

                        <div x-show="thread_id">
                            <div x-init="$watch('thread_id', value => fetchConversation())">
                                <template x-for="(item, index) in inbox" :key="item.id">
                                    <div class="flex flex-row mb-3 item-message" :class="user_auth == item.owner.base.id ? 'justify-end' : 'justify-start'" x-bind:data-id="item.id">
                                        <div class="relative flex flex-shrink-0 w-8 h-8 mr-4">
                                            <img class="object-cover w-full h-full rounded-full shadow-md" :src="item.owner.base.profile_photo_url" alt=""/>
                                        </div>
                                        <div class="grid grid-flow-row gap-2 text-sm messages">
                                            <div class="flex items-center group">
                                                <p class="max-w-xs px-6 py-3 rounded-b-full rounded-r-full lg:max-w-md" x-text="item.body" :class="user_auth == item.owner.base.id ? 'bg-blue-500 text-white' : 'bg-gray-100 text-gray-700'">...</p>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                            </div>
                        </div>
                    </div>

                    <div class="flex-none chat-footer">
                        <div class="flex flex-row items-center p-4">
                            <div class="relative flex-grow">
                                <label>
                                    <input class="w-full py-2 pl-3 pr-10 text-gray-600 transition duration-300 ease-in bg-gray-200 border border-gray-200 rounded-full focus:bg-white focus:outline-none focus:shadow-md" type="text" value="" placeholder="Aa" x-ref="message" x-on:keydown.enter="sendMessage($refs.message.value);$refs.message.value=''"/>
                                </label>
                            </div>
                            <button type="button" class="flex flex-shrink-0 block w-6 h-6 mx-2 text-blue-600 focus:outline-none hover:text-blue-700" @click="sendMessage($refs.message.value);$refs.message.value=''">
                                <img class="w-full h-10 h-full fill-current" src="{{ asset('/images/icon-send.svg') }}" />
                            </button>
                        </div>
                    </div>
                </section>
            </main>
        @endauth
    </div>
</div>
@endsection

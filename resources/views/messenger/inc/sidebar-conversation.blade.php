<section class="flex flex-col flex-none w-24 overflow-auto transition-all duration-300 ease-in-out lg:max-w-sm md:w-2/5">
    <p class="m-3 text-xl font-light font-bold text-gray-800">Users</p>

    <div class="flex flex-row w-0 min-w-full p-2 overflow-auto active-users">
        @foreach ($users as $user)
            <a href="/inbox/{{ $user->id }}">
                <div class="mr-4 text-sm text-center"><div class="p-1 border-4 border-blue-600 rounded-full"><div class="relative flex flex-shrink-0 w-16 h-16">
                    <img class="object-cover w-full h-full rounded-full shadow-md" src="{{ $user->profile_photo_url }}" alt="{{ $user->name }}"/>
                </div></div><p>{{ $user->name }}</p></div>
            </a>
        @endforeach
    </div>

    <div class="flex-none p-4 search-box">
        <form>
            <div class="relative">
                <label>
                    <input class="w-full py-2 pl-10 pr-6 text-gray-600 transition duration-300 ease-in bg-gray-200 border border-gray-200 rounded-full focus:bg-white focus:outline-none focus:shadow-md"
                           type="text" value="" placeholder="Search Messenger"/>
                    <span class="absolute top-0 left-0 inline-block mt-2 ml-3">
                        <svg viewBox="0 0 24 24" class="w-6 h-6">
                            <path fill="#bbb"
                                  d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z"/>
                        </svg>
                    </span>
                </label>
            </div>
        </form>
    </div>

    <div class="flex-1 p-2 overflow-y-scroll contacts" x-data="thread()" x-init="fetchThreads()">
        <template x-for="(item, index) in threads">
            <a x-bind:href="'/inbox/' + item.item.resources.recipient.base.id">
                <div class="relative flex items-center justify-between p-3 rounded-lg hover:bg-gray-100">
                    <div class="relative flex flex-shrink-0 w-16 h-16">
                        <img class="object-cover w-full h-full rounded-full shadow-md" :src="item.resources.recipient.base.profile_photo_url" alt=""/>
                    </div>
                    <div class="flex-auto hidden min-w-0 ml-4 mr-6 md:block">
                        <p x-text="item.name"></p>
                        <div class="flex items-center text-sm text-gray-600">
                            <div class="min-w-0">
                                <p class="truncate" x-text="item.resources.latest_message.body"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </template>
    </div>
</section>

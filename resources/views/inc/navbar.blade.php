<!-- nav bar -->
<nav class="sticky w-full h-auto px-8 bg-gray-200 sm:px-0">
    <div class="flex justify-between px-8 py-4 mx-auto">
        <div class="flex items-center">
            <img class="h-10" src="{{ asset('/images/Facebook-Messenger-Logo-PNG-HD-Image.png') }}" />
            <label class="text-2xl font-light text-gray-800">
                <a href="/">Laravel 8 <span class="font-bold ">Messeger</span></a>
                <small class="text-xs">Developer @TienTN.</small>
            </label>
        </div>
        <ul class="flex items-center text-gray-800">
            <li class="mr-6 font-semibold uppercase">
                @if (Route::has('login'))
                    @auth
                        <span class="hover:text-blue-600">Hello {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}} (ID: {{ Auth::user()->id }}) !</span>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="px-4 py-2 font-bold text-white bg-blue-500 border border-blue-700 rounded hover:bg-blue-700">Logout</button>
                        </form>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 underline dark:text-gray-500 hover:text-blue-600">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline dark:text-gray-500 hover:text-blue-600">Register</a>
                        @endif
                    @endauth
                @endif
            </li>
        </ul>
    </div>
</nav>

import Alpine from 'alpinejs';

window.Alpine = Alpine;
window.axios = require('axios');

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found');
}

window.inbox = () => ({
    user_auth: document.querySelector('#user-auth').value,
    user_id: document.querySelector('#user-id').value,
    user_info: null,
    thread_id: null,
    inbox: [],
    loading: true,
    last_message_id: null,
    fetchUser() {
        axios.get('/api/messenger/privates/recipient/user/' + this.user_id).then(response => {
            this.user_info = response.data.recipient;
            this.thread_id = response.data.thread_id;
            this.loading = false;
        }).catch(function (error) {
            console.log(error);
        });
    },
    sendMessage(message) {
        if (message == '') {
            alert('The message field is required!')
            return;
        }
        if (this.thread_id == null) {
            this.createConversation(message);
        } else {
            let last_message_id = document.querySelector(".item-message:last-child").getAttribute('data-id')
            axios.post('/api/messenger/threads/' + this.thread_id + '/messages', {
                    message: message,
                    temporary_id: last_message_id,
                })
                .then(response => {
                    this.fetchConversation();
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    },
    createConversation(message) {
        axios.post('/api/messenger/privates', {
                message: message,
                recipient_id: this.user_id,
                recipient_alias: 'user',
            })
            .then(response => {
                this.thread_id = response.data.id;
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    fetchConversation() {
        if (this.thread_id == null) {
            return;
        }
        this.isLoading = true;
        fetch('/api/messenger/threads/' + this.thread_id + '/load/')
            .then((res) => res.json())
            .then((data) => {
                this.inbox = data.resources.messages.data.sort((a, b) => (a['created_at'] > b['created_at']) ? 1 : -1);
                this.loading = false;
            });
    },
});

window.thread = () => ({
    threads: [],
    fetchThreads() {
        axios.get('/api/messenger/threads').then(response => {
            this.threads = response.data.data;
        }).catch(function (error) {
            console.log(error);
        });
    },
});

Alpine.start();
